### TAACKdroid!

This is a simple app that uses https://jsonplaceholder.typicode.com/ to fetch
data and show it to the user. It only shows a single user's posts and details
about them.

This was meant as an experiment to test and learn the basics of the Android
Architecture Components and other concepts like Coroutines, DI with Dagger and
the Repository Pattern.