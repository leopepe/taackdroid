package com.leopepe.taackdroid.repositories

import androidx.lifecycle.LiveData
import com.leopepe.taackdroid.database.UserDao
import com.leopepe.taackdroid.models.User
import com.leopepe.taackdroid.network.RetrofitService
import com.leopepe.taackdroid.util.AppConstants.MAIN_USER_ID
import javax.inject.Inject

class UserRepository @Inject constructor(
    private val network: RetrofitService,
    private val userDao: UserDao
) {

    val user: LiveData<User?> = userDao.getUser(MAIN_USER_ID)

    suspend fun refreshUser() {
        val serverData = network.getUserWithId(MAIN_USER_ID)
        userDao.insert(serverData)
    }
}