package com.leopepe.taackdroid.repositories

import androidx.lifecycle.LiveData
import com.leopepe.taackdroid.database.PostDao
import com.leopepe.taackdroid.models.Post
import com.leopepe.taackdroid.network.RetrofitService
import com.leopepe.taackdroid.util.AppConstants.MAIN_USER_ID
import javax.inject.Inject

class PostRepository @Inject constructor(
    private val network: RetrofitService,
    private val postDao: PostDao
) {

    val userPosts: LiveData<List<Post>> = postDao.getPostsFromUser(MAIN_USER_ID)

    suspend fun refreshAllPosts() {
        val serverData = network.getUserPosts(MAIN_USER_ID)
        postDao.insert(serverData)
    }

    fun getPost(id: Int): LiveData<Post?> {
        return postDao.getPost(id)
    }
}