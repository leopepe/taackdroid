package com.leopepe.taackdroid.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.leopepe.taackdroid.models.Post

@Dao
interface PostDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(posts: List<Post>)

    @Query("SELECT * FROM posts WHERE id = :id")
    fun getPost(id: Int): LiveData<Post?>

    @Query("SELECT * FROM posts WHERE userId = :userId")
    fun getPostsFromUser(userId: Int): LiveData<List<Post>>
}