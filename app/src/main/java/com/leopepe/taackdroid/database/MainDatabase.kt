package com.leopepe.taackdroid.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.leopepe.taackdroid.models.Post
import com.leopepe.taackdroid.models.User

@Database(entities = [User::class, Post::class], version = 2, exportSchema = false)
abstract class MainDatabase: RoomDatabase() {

    abstract val userDao: UserDao
    abstract val postDao: PostDao

    companion object {
        const val DATABASE_NAME = "database_name"
    }
}