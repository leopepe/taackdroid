package com.leopepe.taackdroid.userdetails

import androidx.lifecycle.*
import com.leopepe.taackdroid.repositories.PostRepository
import com.leopepe.taackdroid.repositories.UserRepository
import kotlinx.coroutines.launch
import javax.inject.Inject

class UserDetailsViewModel @Inject constructor(
    private val userRepository: UserRepository,
    private val postRepository: PostRepository
): ViewModel() {

    val user = userRepository.user
    val userPosts = postRepository.userPosts

    private val _navigateToPostDetails = MutableLiveData(-1)

    val navigateToPostDetails: LiveData<Int>
        get() = _navigateToPostDetails

    fun onPostClicked(id: Int) {
        _navigateToPostDetails.value = id
    }

    fun doneNavigatingToPostDetails() {
        _navigateToPostDetails.value = -1
    }

    init {
        viewModelScope.launch {
            userRepository.refreshUser()
            postRepository.refreshAllPosts()
        }
    }
}