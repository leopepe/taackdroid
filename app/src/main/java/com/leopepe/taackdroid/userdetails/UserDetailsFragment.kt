package com.leopepe.taackdroid.userdetails


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.leopepe.taackdroid.R
import com.leopepe.taackdroid.databinding.FragmentUserDetailsBinding
import dagger.android.support.DaggerFragment
import javax.inject.Inject


class UserDetailsFragment : DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by viewModels<UserDetailsViewModel> {
        viewModelFactory
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentUserDetailsBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_user_details, container, false
        )

        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel

        viewModel.navigateToPostDetails.observe(viewLifecycleOwner) {
            if (it != -1) {
                this.findNavController().navigate(
                    UserDetailsFragmentDirections.actionUserDetailsFragmentToPostDetailsFragment(it)
                )
                viewModel.doneNavigatingToPostDetails()
            }
        }

        val adapter = PostAdapter(PostClickListener {
            viewModel.onPostClicked(it)
        })

        binding.postsList.adapter = adapter

        // Can it be added as part of the XML? Is there a way to at least preview it?
        binding.postsList.addItemDecoration(
            DividerItemDecoration(context!!, LinearLayoutManager.VERTICAL)
        )

        viewModel.userPosts.observe(viewLifecycleOwner) {
            adapter.submitList(it)
        }

        return binding.root
    }

}
