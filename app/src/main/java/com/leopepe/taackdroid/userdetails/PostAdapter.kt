package com.leopepe.taackdroid.userdetails

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.leopepe.taackdroid.databinding.ListItemPostBinding
import com.leopepe.taackdroid.models.Post

class PostAdapter(val clickListener: PostClickListener): ListAdapter<Post, PostViewHolder>(PostDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder {
        return PostViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
        holder.bind(getItem(position), clickListener)
    }
}

class PostViewHolder private constructor(
    private val binding: ListItemPostBinding
): RecyclerView.ViewHolder(binding.root) {

    companion object {
        fun from(parent: ViewGroup): PostViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding = ListItemPostBinding.inflate(layoutInflater, parent, false)
            return PostViewHolder(binding)
        }
    }

    fun bind(item: Post, clickListener: PostClickListener) {
        binding.post = item
        binding.clickListener = clickListener
        binding.executePendingBindings()
    }

}

class PostDiffCallback: DiffUtil.ItemCallback<Post>() {
    override fun areItemsTheSame(oldItem: Post, newItem: Post): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Post, newItem: Post): Boolean {
        return oldItem == newItem
    }
}

class PostClickListener(val clickListener: (id: Int) -> Unit) {
    fun onClick(id: Int) = clickListener(id)
}