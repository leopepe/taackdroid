package com.leopepe.taackdroid.network

import com.leopepe.taackdroid.models.Post
import com.leopepe.taackdroid.models.User
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface RetrofitService {

    companion object {
        const val BASE_URL = "https://jsonplaceholder.typicode.com/"
    }

    @GET("users/{id}")
    suspend fun getUserWithId(@Path("id") id: Int): User

    @GET("posts")
    suspend fun getUserPosts(@Query("userId") userId: Int): List<Post>

}