package com.leopepe.taackdroid.util

object AppConstants {
    // Arbitrary value just to have a "default" user.
    const val MAIN_USER_ID = 2
    const val REQUEST_TIMEOUT = 5000L
}