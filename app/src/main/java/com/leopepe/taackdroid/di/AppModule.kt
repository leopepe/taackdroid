package com.leopepe.taackdroid.di

import android.content.Context
import androidx.room.Room
import com.leopepe.taackdroid.database.MainDatabase
import com.leopepe.taackdroid.database.MainDatabase.Companion.DATABASE_NAME
import com.leopepe.taackdroid.database.PostDao
import com.leopepe.taackdroid.database.UserDao
import com.leopepe.taackdroid.network.RetrofitService
import com.leopepe.taackdroid.network.RetrofitService.Companion.BASE_URL
import com.leopepe.taackdroid.util.AppConstants.REQUEST_TIMEOUT
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
object AppModule {

    @JvmStatic
    @Singleton
    @Provides
    fun provideOkHttpClient(): OkHttpClient = OkHttpClient()
        .newBuilder()
        .callTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
        .build()

    @JvmStatic
    @Singleton
    @Provides
    fun provideRetrofitService(okHttpClient: OkHttpClient): RetrofitService {
        return Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(RetrofitService::class.java)
    }

    @JvmStatic
    @Provides
    @Singleton
    fun provideDatabase(context: Context): MainDatabase {
        return Room.databaseBuilder(
            context.applicationContext,
            MainDatabase::class.java,
            DATABASE_NAME
        )
        .fallbackToDestructiveMigration()
        .build()
    }

    @JvmStatic
    @Provides
    @Singleton
    fun provideUserDao(database: MainDatabase): UserDao = database.userDao

    @JvmStatic
    @Provides
    @Singleton
    fun providePostDao(database: MainDatabase): PostDao = database.postDao

}
