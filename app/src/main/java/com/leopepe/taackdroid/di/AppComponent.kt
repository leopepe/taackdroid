package com.leopepe.taackdroid.di

import android.content.Context
import com.leopepe.taackdroid.TaackdroidApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AppModule::class,
        AndroidInjectionModule::class,
        UserDetailsModule::class,
        PostDetailsModule::class
    ]
)
interface AppComponent: AndroidInjector<TaackdroidApplication> {
    @Component.Factory
    interface Factory {
        fun create(@BindsInstance applicationContext: Context): AppComponent
    }
}