package com.leopepe.taackdroid.di

import androidx.lifecycle.ViewModel
import com.leopepe.taackdroid.postdetails.PostDetailsFragment
import com.leopepe.taackdroid.postdetails.PostDetailsViewModel
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
interface PostDetailsModule {

    @ContributesAndroidInjector(modules = [
        ViewModelBuilder::class
    ])
    fun postDetailsFragment(): PostDetailsFragment

    @Binds
    @IntoMap
    @ViewModelKey(PostDetailsViewModel::class)
    fun bindViewModel(viewModel: PostDetailsViewModel): ViewModel
}