package com.leopepe.taackdroid.di

import androidx.lifecycle.ViewModel
import com.leopepe.taackdroid.userdetails.UserDetailsFragment
import com.leopepe.taackdroid.userdetails.UserDetailsViewModel
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
interface UserDetailsModule {

    @ContributesAndroidInjector(modules = [
        ViewModelBuilder::class
    ])
    fun userDetailsFragment(): UserDetailsFragment

    @Binds
    @IntoMap
    @ViewModelKey(UserDetailsViewModel::class)
    fun bindViewModel(viewModel: UserDetailsViewModel): ViewModel
}