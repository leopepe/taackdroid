package com.leopepe.taackdroid.postdetails

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.leopepe.taackdroid.models.Post
import com.leopepe.taackdroid.repositories.PostRepository
import kotlinx.coroutines.launch
import javax.inject.Inject

class PostDetailsViewModel @Inject constructor(
    private val postRepository: PostRepository
): ViewModel() {

    var post: LiveData<Post?>? = null

    fun setPostId(id: Int) {
        viewModelScope.launch {
            post = postRepository.getPost(id)
        }
    }
}