package com.leopepe.taackdroid.postdetails


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs

import com.leopepe.taackdroid.R
import com.leopepe.taackdroid.databinding.FragmentPostDetailsBinding
import dagger.android.support.DaggerFragment
import javax.inject.Inject


class PostDetailsFragment : DaggerFragment() {

    private val args: PostDetailsFragmentArgs by navArgs()

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by viewModels<PostDetailsViewModel> {
        viewModelFactory
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentPostDetailsBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_post_details, container, false
        )

        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel

        viewModel.setPostId(args.postId)

        return binding.root
    }


}
